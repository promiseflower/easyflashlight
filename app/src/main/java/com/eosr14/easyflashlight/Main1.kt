//package com.eosr14.easyflashlight
//
//import android.Manifest
//import android.app.ActivityManager
//import android.app.Notification
//import android.app.NotificationManager
//import android.app.PendingIntent
//import android.app.Service
//import android.content.BroadcastReceiver
//import android.content.Context
//
//import android.content.Intent
//import android.content.IntentFilter
//import android.content.pm.PackageManager
//import android.hardware.Camera
//import android.hardware.camera2.CameraAccessException
//import android.hardware.camera2.CameraManager
//import android.os.BatteryManager
//import android.os.Build
//import android.os.Bundle
//import android.os.IBinder
//import android.support.v4.app.ActivityCompat
//import android.support.v4.app.NotificationCompat
//import android.support.v7.app.AppCompatActivity
//import android.view.View
//import android.widget.Button
//import android.widget.RemoteViews
//import android.widget.TextView
//import android.widget.Toast
//
//import java.util.ArrayList
//
//class Main1 : AppCompatActivity() {
//
//    private var mTextView: TextView? = null
//    private var mButton: Button? = null
//    private var mBtnTaskNoti: Button? = null
//
//    private var isFlashOn: Boolean = false
//
//    private var mCamera: Camera? = null
//    private var parameters: Camera.Parameters? = null
//    private var camManager: CameraManager? = null
//
//    private val REQ_PERMISSION_CODE = 1001
//
//    /******************************************************************************************************
//     * Events
//     */
//    private val mOnClickListener = View.OnClickListener { v ->
//        if (v === mButton) {
//            if (isFlashOn) {
//                turnFlashlightOff()
//            } else {
//                turnFlashlightOn()
//            }
//        } else if (v === mBtnTaskNoti) {
//
//        }
//    }
//
//    private val mBroadcastReceiver = object : BroadcastReceiver() {
//        override fun onReceive(context: Context, intent: Intent) {
//            // Get the battery scale
//            val scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
//
//            // get the battery level
//            val level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
//            mTextView!!.text = level.toString() + "%"
//
//            setNotification("배터리 현황 : " + level.toString() + "%")
//        }
//    }
//
//    private val buttonBroadcastReceiver = object : BroadcastReceiver() {
//        override fun onReceive(context: Context, intent: Intent) {
//            Toast.makeText(context, "notification Button Clicked", Toast.LENGTH_LONG).show()
//        }
//    }
//
//    /******************************************************************************************************
//     * LifeCycle
//     */
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
//
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            // ...
//        } else {
//            // 머시멜로우 6.0 이상
//            checkPermissions()
//        }
//
//        val iFilter1 = IntentFilter()
//        iFilter1.addAction("BUTTON_POWER")
//        registerReceiver(buttonBroadcastReceiver, iFilter1)
//
//        val iFilter2 = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
//        registerReceiver(mBroadcastReceiver, iFilter2)
//
//        mTextView = findViewById<View>(R.id.tv_battery) as TextView
//
//        mButton = findViewById<View>(R.id.btn_power) as Button
//        mButton!!.setOnClickListener(mOnClickListener)
//
//        mBtnTaskNoti = findViewById(R.id.btn_task_noti) as Button
//        mBtnTaskNoti!!.setOnClickListener(mOnClickListener)
//    }
//
//    /******************************************************************************************************
//     * Methods
//     */
//    private fun checkPermissions() {
//        // 퍼미션 체크
//        val permissions = arrayOf(Manifest.permission.CAMERA)
//
//        val needPermissions = PermissionUtil.checkDeniedPermissions(this@MainActivity, *permissions)
//        if (needPermissions.size > 0) {
//            android.util.Log.d("eosr14", "checkPermissions 부여받지 못한 권한이 1개 이상일 경우")
//            ActivityCompat.requestPermissions(this@MainActivity, needPermissions, REQ_PERMISSION_CODE)
//        } else {
//            android.util.Log.d("eosr14", "checkPermissions 부여받지 못한 권한이 없을 경우")
//        }
//    }
//
//    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
//        android.util.Log.d("eosr14", "onRequestPermissionsResult - " + grantResults.toString())
//        val deniedPermissions = ArrayList<String>()
//        for (i in permissions.indices) {
//            val permission = permissions[i]
//            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
//                deniedPermissions.add(permission)
//            }
//        }
//
//        if (deniedPermissions.isEmpty()) {
//            android.util.Log.d("eosr14", "onRequestPermissionsResult 권한 허가")
//        } else {
//            android.util.Log.d("eosr14", "onRequestPermissionsResult 권한 거부")
//            Toast.makeText(this@MainActivity, "권한을 얻지 못했습니다.", Toast.LENGTH_LONG).show()
//        }
//    }
//
//    private fun turnFlashlightOn() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            try {
//                camManager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
//                var cameraId: String? = null // Usually front camera is at 0 position.
//                if (camManager != null) {
//                    cameraId = camManager!!.cameraIdList[0]
//                    camManager!!.setTorchMode(cameraId!!, true)
//                }
//            } catch (e: CameraAccessException) {
//                e.printStackTrace()
//            }
//
//        } else {
//            mCamera = Camera.open()
//            parameters = mCamera!!.parameters
//            parameters!!.flashMode = Camera.Parameters.FLASH_MODE_TORCH
//            mCamera!!.parameters = parameters
//            mCamera!!.startPreview()
//        }
//        isFlashOn = true
//    }
//
//    private fun turnFlashlightOff() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            try {
//                val cameraId: String
//                camManager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
//                if (camManager != null) {
//                    cameraId = camManager!!.cameraIdList[0] // Usually front camera is at 0 position.
//                    camManager!!.setTorchMode(cameraId, false)
//                }
//            } catch (e: CameraAccessException) {
//                isFlashOn = false
//                e.printStackTrace()
//            }
//
//        } else {
//            mCamera = Camera.open()
//            parameters = mCamera!!.parameters
//            parameters!!.flashMode = Camera.Parameters.FLASH_MODE_OFF
//            mCamera!!.parameters = parameters
//            mCamera!!.stopPreview()
//        }
//        isFlashOn = true
//    }
//
//    private fun setNotification(batteryLevel: String) {
//
//        val notificationLayout = RemoteViews(packageName, R.layout.custiom_notification)
//
//        // notification's battery text
//        notificationLayout.setTextViewText(R.id.tv_noti_battery, batteryLevel)
//
//        val intent1 = Intent(this, MainActivity::class.java)
//        val pendingIntent1 = PendingIntent.getActivity(this, 0, intent1, PendingIntent.FLAG_ONE_SHOT)
//
//        //        Intent intent2 = new Intent("BUTTON_POWER");
//        val intent2 = Intent(this@MainActivity, FlashLightService::class.java)
//        val pendingIntent2 = PendingIntent.getService(this, 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT)
//        //        PendingIntent pendingIntent2 = PendingIntent.getBroadcast(this, 0, intent2,
//        //                PendingIntent.FLAG_UPDATE_CURRENT);
//
//        notificationLayout.setOnClickPendingIntent(R.id.btn_noti_power, pendingIntent2)
//
//        /**
//         * 먼저 NotificationCompat Builder를 선언한다.
//         * Builder의 경우 접혔을 때 노티바에 표시할 컨텐츠를 설정한다.
//         */
//        val builder = NotificationCompat.Builder(this)
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setOngoing(true)
//                .setStyle(NotificationCompat.DecoratedCustomViewStyle())
//                .setCustomContentView(notificationLayout)
//                .setContentIntent(pendingIntent1) as NotificationCompat.Builder
//
//        /**
//         * 두 손가락으로 아래로 드래그 했을 때 표시된 컨텐츠를 설정한다.
//         */
//        //        if (!TextUtils.isEmpty(imagePath)) {
//        //            Bitmap bigPictureBitmap = ImageLoader.getInstance().loadImageSync(imagePath);
//        //            if (bigPictureBitmap != null) {
//        //                NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle(builder); //상단의 빌더를 인자로 받음..
//        //                bigPictureStyle.bigPicture(bigPictureBitmap) //상단의 비트맵을 넣어준다.
//        //                        .setBigContentTitle(notiTitle) //열렸을때의 타이틀
//        //                        .setSummaryText(message); //열렸을때의 Description
//        //            }
//        //        }
//
//        val notificationId = 0
//        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//        notificationManager.notify(notificationId, builder.build())
//    }
//
//
//    class FlashLightService : Service() {
//
//        private var isFlashOn: Boolean = false
//
//        private var mCamera: Camera? = null
//        private var parameters: Camera.Parameters? = null
//        private var camManager: CameraManager? = null
//
//        override fun onBind(intent: Intent): IBinder? {
//            return null
//        }
//
//        override fun onCreate() {
//            super.onCreate()
//        }
//
//        override fun onStart(intent: Intent, startId: Int) {
//            super.onStart(intent, startId)
//            android.util.Log.d("eosr14", "flashLightService start")
//
//            if (isFlashOn) {
//                turnFlashlightOff()
//            } else {
//                turnFlashlightOn()
//            }
//        }
//
//        override fun onDestroy() {
//            super.onDestroy()
//        }
//
//        private fun turnFlashlightOn() {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                try {
//                    camManager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
//                    var cameraId: String? = null // Usually front camera is at 0 position.
//                    if (camManager != null) {
//                        cameraId = camManager!!.cameraIdList[0]
//                        camManager!!.setTorchMode(cameraId!!, true)
//                    }
//                } catch (e: CameraAccessException) {
//                    e.printStackTrace()
//                }
//
//            } else {
//                mCamera = Camera.open()
//                parameters = mCamera!!.parameters
//                parameters!!.flashMode = Camera.Parameters.FLASH_MODE_TORCH
//                mCamera!!.parameters = parameters
//                mCamera!!.startPreview()
//            }
//            isFlashOn = true
//        }
//
//        private fun turnFlashlightOff() {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                try {
//                    val cameraId: String
//                    camManager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
//                    if (camManager != null) {
//                        cameraId = camManager!!.cameraIdList[0] // Usually front camera is at 0 position.
//                        camManager!!.setTorchMode(cameraId, false)
//                    }
//                } catch (e: CameraAccessException) {
//                    isFlashOn = false
//                    e.printStackTrace()
//                }
//
//            } else {
//                mCamera = Camera.open()
//                parameters = mCamera!!.parameters
//                parameters!!.flashMode = Camera.Parameters.FLASH_MODE_OFF
//                mCamera!!.parameters = parameters
//                mCamera!!.stopPreview()
//            }
//            isFlashOn = true
//        }
//    }
//
//}
